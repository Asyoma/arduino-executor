
import { SerialPort } from "serialport";
import { ReadlineParser } from "@serialport/parser-readline";
import { exec } from "child_process";
import { platform } from "os";
import { CONSOLE_LOG } from "./consoleLog";
export class ArduinoSerialport{
    private arduinoPort:string="COM4";
    private isDev:boolean=false;
    constructor(arduinoPort:string|null = null,isDev=false){
        if(arduinoPort!= null)
        this.arduinoPort=arduinoPort;
        this.isDev=isDev;
        this.init();
    }
    private init(){
        CONSOLE_LOG.info("Init");
        const port = new SerialPort( {
            path:this.arduinoPort,
            baudRate: 9600,
        });
        port.on("error",(err:Error)=>{
            CONSOLE_LOG.info(err.message);
            setTimeout(()=>{
                this.init();
            },3000);
        });
        port.on("open",()=>{
            try{
                const parser = port.pipe(new ReadlineParser({ delimiter: '\r\n' }))
                parser.on("error",(e:Error)=>{
                    CONSOLE_LOG.info(`Error: ${e.message}`);
                });
                parser.on('data', (chunk:any)=>{
                    try{
                        const x = (chunk as string).trim();
                        const obj = JSON.parse(x);
                        CONSOLE_LOG.info(`Object: ${x}`);
                        console.log(chunk);
                        if(obj["arduino-command"] != undefined && obj["arduino-command"] != null){
                            const cmd = (c:string)=>{
                                exec(c, (error, stdout, stderr) => {
                                    if (error) {
                                        console.log(`error: ${error.message}`);
                                        return;
                                    }
                                    if (stderr) {
                                        console.log(`stderr: ${stderr}`);
                                        return;
                                    }
                                    console.log(`stdout: ${stdout}`);
                                });
                            }
                            if(obj["arduino-command"] == "shutdown"){
                                switch(platform()){
                                    case "win32":
                                        if(this.isDev == true){
                                            cmd("notepad");
                                        }
                                        else{
                                            cmd("shutdown /s");
                                        }
                                        
                                    break;
                                    default:
                                        if(this.isDev == true){
                                            cmd("kate");
                                        }
                                        else{
                                            cmd("poweroff");
                                        }
                                        
                                    break;
                
                                }
                            }
                            else{
                                cmd(obj["arduino-command"]);
                            }
                            
                            
                        }
                    }
                    catch(e:any){
                        //console.log(`error: ${e}`);
                        const x = (chunk as string).trim();
                        CONSOLE_LOG.info(x);
                    }
                });
            }
            catch(e:any){
                setTimeout(()=>{
                    this.init();
                },3000);
            }
        });
        
        
        
        
        
            
        
        
        port.on("close",()=>{
            CONSOLE_LOG.info("Arduino port closed");
            setTimeout(()=>{
                this.init();
            },3000);
        })
    }
}