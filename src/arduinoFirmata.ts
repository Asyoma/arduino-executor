import * as five from "johnny-five";
export class ArduinoFirmata{
    constructor(firmataPort:string| null,firmataOpen:boolean,cb:{():void}){
        if(firmataPort != null){
            var board = new five.Board();

            board.on("ready", function() {
                cb();
                var relay = new five.Relay(10);
                if(firmataOpen){
                    relay.open();
                }
                else{
                    relay.close();
                }
                
            });
        }
        else{
            cb();
        }
    }
}