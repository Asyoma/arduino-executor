import { CONSOLE_LOG } from "./consoleLog";
import { Main } from "./main";

class Index{
    constructor(){
        let isDev = false;
        let arduinoPort:string = "COM4";
        let firmataPort:string|null = null;
        let firmataOpen:boolean = true;
        process.argv.forEach( (val:string, index:number)=> {
            
            try{
                const parts=val.split("=");
                switch(parts[0]){
                    case "--arduino-port":
                        try{
                            arduinoPort=parts[1];
                        }
                        catch(e){
                            CONSOLE_LOG.error.red(e);
                        }
                    break;
                    case "--firmata-port":
                        try{
                            firmataPort=parts[1];
                        }
                        catch(e){
                            CONSOLE_LOG.error.red(e);
                        }
                    break;
                    case "--firmata-open":
                        firmataOpen=true;
                    break;
                    case "--firmata-close":
                        firmataOpen=false;
                    break;
                    case "--is-dev":
                        isDev=true;
                    break;
                }
                if(index == process.argv.length-1){
                    new Main(arduinoPort,firmataPort,firmataOpen,isDev);
                }
              }
              catch(err){
                if(index == process.argv.length-1){
                    new Main(arduinoPort,firmataPort,firmataOpen,isDev);
                }
              }
        });
        
    }
}


new Index();