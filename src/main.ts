import { ArduinoFirmata } from "./arduinoFirmata";
import { ArduinoSerialport } from "./arduinoSerialport";

export class Main{
    constructor(arduinoPort:string,firmataPort:string|null,firmataOpen:boolean,isDev:boolean){
        new ArduinoFirmata(firmataPort,firmataOpen,()=>{
            new ArduinoSerialport(arduinoPort,isDev);
        });
       
    }
}