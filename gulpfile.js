
const exec = require('child_process').exec;
const gulp = require('gulp'); 
const fs = require("fs");
const rimraf = require('rimraf')
const path = require('path');
const rename = require("gulp-rename");


function pjson(){
let rawdata = fs.readFileSync('./package.json');
return JSON.parse(rawdata);
}

gulp.task('build', function (cb) {
    const display_name = pjson().display_name;
    const name = pjson().name;
    const version = pjson().version;

    const fileName_win= display_name+"_"+version+".exe";
    const fileName_linux = display_name+"_"+version+".run";

    
    const action = async ()=>{

        await gulp.src("./build/"+name+"-linux")
        .pipe(rename(fileName_linux))
        .pipe(gulp.dest("./build"));

        await gulp.src("./build/"+name+"-win.exe")
        .pipe(rename(fileName_win))
        .pipe(gulp.dest("./build"));

        setTimeout(()=>{
            fs.unlink("./build/"+name+"-win.exe",()=>{
                fs.unlink("./build/"+name+"-linux",()=>{
    
                    cb();
                })
            });
        },1000);
    }
    //serialport-build

    exec("gulp serialport-build",(err,stdout,stderr)=>{
        exec("gulp make",(err,stdout,stderr)=>{
            rimraf("./build",  ()=> { 
                console.log("Cleared ./build");
                fs.mkdir("./build",()=>{
                    exec("pkg --compress GZip .",(err,stdout,stderr)=>{
                        action();
                    });
                });
                
                
            });
        });
    });
    

    

});

/**
 * 
 */


gulp.task('make', function (cb) {
    const action = ()=>{
        exec("tsc",function(err,stdout,stderr){
            console.log(stdout);
            console.error(stderr);
            
    
            cb();
        });
    }
    
    if(!fs.existsSync("./dist")){
        fs.mkdirSync("./dist");
        console.log("Created ./dist");
        action();
    }
    else{
        rimraf("./dist", function () { 
            console.log("Cleared ./dist");
            fs.mkdirSync("./dist");
            action();
         });
        
    }
    

   
})

gulp.task('serialport-build', function (cb) {
    exec("node-gyp configure build",{
        cdw: "node_modules/@serialport/bindings-cpp"
    },function(err,stdout,stderr){
            console.log(stdout);
            console.error(stderr);
            
    
            cb();
        });
    

   
})

