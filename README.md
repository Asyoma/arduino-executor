PreRequirements:
- Python 2.7
- NodeJS 18

Installation:
- npm install
- npm i -g pkg
- npm i serialport
- npm i @serialport/parser-readline

If Arduino on COM4:
 - npm start 
else add argument 
- --arduino-port=<arduino port here>
